# Tasker

* License  : GPLv3
* ProjectURL: https://bitbucket.org/Bigelow_BioInfo/task-manager

## Dependencies
* Python >= 2.6
* Unittest2 (to run tests)

## Install

    # clone the repo
    $ https://Bigelow_BioInfo@bitbucket.org/Bigelow_BioInfo/task-manager.git

    # run/inspect test_tasker.py for example usage 
    # (assuming you don't want to install this, just move the test up one level to run)

    # specify db site in test_tasker 
    # if your Linux image does not have the default /dev/shm/ ramdisk

## Original prompt

    Design and build a program that implements a "task manager". This
    task manager features two primary functions: load a shell script
    into a database, and run all shell scripts in that database. You
    might want to build such a task manager if you need to execute a
    set of programs at a later date, or if you want someone else to run
    a set of your programs because they have different permissions.

    The design of this task manager is entirely up to you. You may use
    whatever programming language and database you are most comfortable
    with. You may also incorporate any UNIX-related tools you think may
    come in handy (for example, to schedule periodic jobs). Think about
    ways the basic load/run API might be extended to run from a command
    line interface or a GUI. Also, think about ways to track the
    completion of shell scripts that take some time to run. (Here at
    Mount Sinai, it is not uncommon to run programs that take hours,
    or even days, to finish.) Could you think of a way, for example,
    to send an automated email any time a script is completed?

    This assignment should take between two and four hours, depending
    on the programming language you use and how detailed you want your
    solution to be. Don't worry about implementing absolutely every
    feature you think of; it's just as good to provide a short explanation
    of how you might implement a new feature given your design.

## Writeup

The long running task / group of tasks is a recurrent problem in Bioinformatics research and production development.
Before I begin talking about design decisions, I should list some assumptions I made about my computing environment.

### Assumptions about environment

* we are operating in a multi-cpu virtual machine of some kind, to make the problem portable/ agnostic of underlying grid architecture.
* any scripts we intend to call with this are globally accessible- in the path, or fully specified by the user (i.e. no rsync or scp has to occur to find the actual script)
* there are enough CPUs and IOPs such that launching all tasks in parallel is a good idea
* I assumed by "load into database" you mean, reference the script location, not shove the entire script as a blob into a text field- this latter doesn't seem a good practice.
* This script/database isn't meant for multiple users 


### Discussion

### Controller style watcher as opposed to cron

This decision is relatively arbitrary.  One could easily add a script to cron that opens the database I've described, and use ps or similar to poll the process IDs (similar to the "cleanup failure" method I describe in the code) and update their status.
A watcher can be killed by OOM errors and superusers, so it doesn't necessarily replace a cron process in a production environment.  On the other hand, this program
can potentially do work entirely on its own, without needing to add a crontask, and the additional overhead is negligible for a process that sleeps 99% of the time.
Either the hypothetical cron script or the watcher setup I've coded can easily email the user, given the schema I've designed.

As the software is written now, it will hang until completion.  One could simply place this controller in the background, or launch through screen, but with a little more work it could fork the watching thread and return control to the user immediately.

### Cron vs watcher if the load is scaled up

It is important that the watcher disconnect from the database, if we are going to have significant numbers of watchers at one time.  In that case, though, the watcher could become a permanent process, and oversee all jobs as a "super watcher" instead of 1:1 watcher:job_set.  This would be beneficial when coupled with a web server like I've described below-- in contrast to cron, this watcher would have relatively instant updates on the state of processing, and that process could expose a RESTful interface both to spawn new jobs, and report job status.

### A third option

Another interesting way to approach this would be to use Linux's inotify capability via pyinotify. One could imagine setting up standard STDOUT and STDERR redirects for all programs (much like LSF does), and registering those files or their containing directories with a watcher.  When both STDOUT and STDERR are closed, the process has completed, and you can mail the user links to those files.  My intuition is this style of watcher would let you trigger events more real-time than either of the other two options, but that is of limited utility, if we're talking instantly versus 1-5 minute delay.

### Multiple users

It would be straightforward to extend this schema to multiple users-- simply add a column in the "task" table that records the username that added the task.  If the intent it to share workflows or repetitive tasks, that might not be so good, since it would lead to people referring to workflows by the people who put them in the database, e.g. "run the charris workflow".   In that case, one might instead opt for a short phrase to act as a key, e.g.  "DownloadNewDatabases" -- humans will find this just as easy to remember, and it's much clearer.

### Email users on completion

I've skeletoned in this functionality, all that's missing is the email address and sendmail command/mail formatting.   A user will get a report for every task launched-  this could easily be changed to be 1 per "job group" instead.

### Command line access

I've included some simple bin/ scripts that allow command line access to the tool

### GUI access

A lightweight web framework would be ideal for the uses described.  Good choices are itty, flask, etc.  One can add lightweight html pages that allow views of the tasks, or 
(my preference) make the web framework entirely the Model.  Another computer can use the RESTful hooks for both submitting and polling processes to generate pretty views. 
That will keep the load on the watcher process relatively light.

### Change the underlying compute architecture

This program could still work under a more distributed computing type architecture, for example LSF.  Running a job would become a "bsub" command instead of a subprocess call, and the job polling would turn into parsing "bjobs" periodically.  LSF already implements mails on job completion, so one just needs to pass the email user along to it.

### Database choice / design

I chose SQLite for this exercise, because it's easy to set up, and it's throughput and capabilities are a good match for the problem as described.  In a multi user scenario, it would be far less appealing. Properly hiding SQLite behind SQL Alchemy or another ORM would make the database choice agnostic, which is probably desirable depending on the projected lifetime of this tool.  

The schema of the "task" table is ad hoc for this problem, but the "task execution" schema is aping other workflow engines and tools I've coded or worked with in the past.
