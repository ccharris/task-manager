from setuptools import setup

set(name='tasker',
        verion='0.1',
        description="Task Manager for repetitive tasks",
        url='localhost',
        author='Chris Haris',
        author_email = 'charris@bigelow.org',
        license="GPLv3",
        packages=['tasker'],
        zip_safe=False)

