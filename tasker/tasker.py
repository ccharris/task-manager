#!/usr/bin/env python

import time, datetime
import subprocess
import sys, os, logging
import sqlite3

class TaskExecution:
    #in real life, we might use SQLAlchemy to make our objects more database agnostic
    #given the use cases listed, "run this set of tasks"
    #and "let another user run my tasks"
    #SQLite and lightweight objects make sense
    #if this were going to develop into a full scale workflow engine 
    #a real database, or a home in the production database
    #would be more appropriate
    def __init__(self, cmd, email_user=None):
        self.id=None
        self.pid=None
        self.start_time=None
        self.stop_time=None
        self.status=None
        self.exit_code=None
        self.stdout=None
        self.stderr=None
        self.command=cmd
        self.email_user=email_user
        self.process=None

    @classmethod
    def fromlist(cls, list):
        task = TaskExecution(list[8].split(" "))
        task.id=list[0]
        task.pid = list[1]
        task.start_time = list[2]
        task.stop_Time = list[3]
        task.status=list[4]
        task.exit_code=list[5]
        task.stdout=list[6]
        task.stderr=list[7]
        task.email_user=list[10]
        return task
    @classmethod
    def fromdict(cls, **dict):
        task = TaskExecution(dict['command'])
        task.__dict__.update(dict)
        return task


class TaskManager:
    def prep_db(self, path=None):
        if not path:
            path = "/tmp/temp.db"
        if(os.path.exists(path)):
            os.unlink(path)
        conn = sqlite3.connect(path)
        c = conn.cursor()
        c.execute("CREATE TABLE task_execution (id INTEGER PRIMARY KEY, pid INTEGER, start_time TEXT,  stop_time TEXT, status TEXT, exit_code INTEGER, stdout TEXT, stderr TEXT, command TEXT, email_user TEXT);");
        c.execute("CREATE TABLE task (id INTEGER PRIMARY KEY, SCRIPT_LOCATION);");
        conn.commit()
        return conn

    def __init__(self, db=None, log_location=None):
        self.log_location=log_location
        self.logger = self.prep_logger(log_location, "TaskManager")
        if(db and os.path.exists(db)):
                self.db=sqlite3.connect(db)
                #in real life we'd probably check to make sure it had tables
                #or otherwise make sure it would work before starting
        else:
            self.db=self.prep_db(db)
        self.db.row_factory=sqlite3.Row

    def log_task(self, task):
        data = [ task.id, task.pid, task.start_time, task.stop_time, task.status, task.exit_code, task.stdout, task.stderr, task.command, task.email_user]
        c = self.db.cursor()
        c.execute("INSERT OR REPLACE INTO task_execution VALUES (?,?,?,?,?,?,?,?,?,?)", data)
       # self.logger.info("logging %s", " ".join(map(str, data)))
        self.db.commit()
        task.id=c.lastrowid
        return c.lastrowid

    def run_task(self, task):
        try:
            task.process = subprocess.Popen(task.command, shell=True)
            self.logger.info("Launched %s [PID=%d]",task.command, task.process.pid)
        except Exception:
            self.logger.warning("Problem running task %s", task.command)
            #email user and cancel rest of task
            return None
        task.pid=task.process.pid
        task.status="Running"
        task.start_time=datetime.datetime.now().isoformat()
        self.log_task(task)
        return task

    def finish_task(self, task):
        self.logger.info("Finishing %s", task.command)
        task.stop_time = datetime.datetime.now().isoformat()
        task.status="Finished"
        task.exit_code = task.process.returncode
        self.log_task(task)
    def _get_tasks_to_run(self):
        c = self.db.cursor()
        c.execute("Select * from task")
        return [ x['SCRIPT_LOCATION'] for x in c.fetchall() ] 
    def get_running_tasks(self):
        c = self.db.cursor()
        c.execute("Select * from task_execution where status='Running'")
        return [ TaskExecution.fromdict(**x) for x in c.fetchall()]
    def add_task_to_run(self, task):
        c = self.db.cursor()
        full_script_path = os.path.abspath(task)
        if not os.path.exists(full_script_path):
            self.logger.warning("Cannot find script %s... ignoring submission" % task)
            return 0
        c.execute("INSERT INTO TASK VALUES (?,?)", [None, full_script_path])
        self.db.commit()
        return 1
    def prep_logger(self, log_location, name):
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        if not logger.handlers:
            formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
            if(log_location):
                file_name = os.path.join(log_location, name + ".log")
                handler=logging.FileHandler(file_name)
                handler.setFormatter(formatter)
                handler.setLevel(logging.DEBUG)
                logger.addHandler(handler)
            console = logging.StreamHandler()
            console.setFormatter(formatter)
            console.setLevel(logging.DEBUG)
            logger.addHandler(console)
        return logger

    def run_all_tasks(self):
        scripts = self._get_tasks_to_run();
        tasks_to_check = []
        for script in scripts:
            self.logger.info("Attempting to launch %s...", script)
            te = TaskExecution(script)
            tasks_to_check.append(self.run_task(te))
        while(1):
            time.sleep(1) #probably more like 60 or 600 for real life
            if not len(tasks_to_check):
                break;
            for task in tasks_to_check[:]:
                if task.process.poll() != None: #none means still going
                    self.finish_task(task)
                    tasks_to_check.remove(task)

    def clean_up_failures(self):
        return true
        #the TaskExecution objects store the actual subprocess events
        #which makes polling for finishing easy
        #we get one 'controller' job per set of tasks,
        #which is desirable given the use cases described for this software
        #if a 'controller' job fails, we've still recorded the PIDs
        #and can poll them manually with "ps" or similar
        #and recover from the failure
        #this function would do that-- we could call it anytime a new one of these is started












