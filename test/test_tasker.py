#!/usr/bin/env python
import tasker
import unittest2, os, time

test_db_path = "/dev/shm/temp_testing.db"

class TestTasker(unittest2.TestCase):

    def setUp(self):
        self.tm = tasker.TaskManager(db=test_db_path,log_location="./")
    def tearDown(self):
        os.unlink(test_db_path)
    def testSuccessfulLoading(self):
        return_code = self.tm.add_task_to_run("./example_bin/test_script.sh")
        self.assertEqual(return_code, 1)
    def testFailureLoading(self):
        return_code = self.tm.add_task_to_run("./example_bin/not_a_real_script.sh")
        self.assertEqual(return_code, 0)
    def testLoadandRun(self):
        return_code = self.tm.add_task_to_run("./example_bin/test_touch_script.sh")
        return_code = self.tm.add_task_to_run("./example_bin/test_script.sh")
        self.assertEqual(return_code, 1)

        self.assertEqual(len(self.tm._get_tasks_to_run()), 2)
        self.tm.run_all_tasks()
        time.sleep(5) #give it a chance to execute
        #paired with test_touch_script.sh, edit it too if you change this
        self.assertEqual(os.path.exists("/tmp/foo"), 1) 

        

suite = unittest2.TestLoader().loadTestsFromTestCase(TestTasker)
unittest2.TextTestRunner(verbosity=2).run(suite)

