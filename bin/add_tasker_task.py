#!usr/bin/env python

import tasker, argparse, sys



if __name__=='__main__':
    parser = argparse.ArgumentParser(description="Add a job to tasker")
    parser.add_argument('--shell-script=', dest="script", action="store")
    parser.add_argument('--db-location=', dest='db', action='store')
    args=parser.parse_args()
    if not args.script:
        parser.print_help()
        print >>sys.stderr, "\nPlease supply a script for upload\n"
        sys.exit(1)
    if not args.db:
        parser.print_help()
        print >>sys.stderr, "\nPlease supply a db to upload to\n"
        sys.exit(1)
    tm = tasker.TaskManager(db=args.db)
    tm.add_task_to_run(args.script)
