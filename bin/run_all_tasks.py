#!usr/bin/env python

import tasker, argparse, sys



if __name__=='__main__':
    parser = argparse.ArgumentParser(description="Add a job to tasker")
    parser.add_argument('--db-location=', dest='db', action='store')
    #in a real environment, we wouldnt supply db location, it would
    #probably reside in a config file somewhere
    args=parser.parse_args()
    if not args.db:
        parser.print_help()
        print >>sys.stderr, "\nPlease supply a db to run\n"
        sys.exit(1)
    tm = tasker.TaskManager(db=args.db)
    tm.run_all_tasks()
